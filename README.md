# Hemoglobin Laboratory Test Reduction

This gitlab resipotory implement a deep learning model for hemoglobin lab test reduction, which targets at pediatric patients hopitalized in intensive care units (ICU) with solid spleen and liver laceration.

# Background

Undertaking unnecessary laboratory tests is identified as an issue in clinical activities. Unnecessary laboratory tests not only increase healthcare costs as they have wasted plenty of clinical resources, but also increase the risk of patient morbidity and mortality. However, discovering specific laboratory test that should not be conducted in the future is challenging. 
An appropriate laboratory test reduction policy should account for the trade-off between clinical satisfaction and high accuracies of the prediction. Recent studies have shown that machine learning approaches are effective to offer medial solutions to estimate future outcomes. 

# Workflow
1. Develop a deep learning model to jointly learn temporal features and static features
2. If the deep learning model can predict the next value, we can disregard the next laboratory test. 3.Otherwise, we will continue to check the laboratory value
3. Predictive accuracy is measured for the reduced laboratory tests 


# Input Features
1. Blood drawing laboratory tests on all visits
- Hemoglobin (Hgb)
- Hct
- Platelet Count (Plt)
- White Blood Cells (WBC)
2. Common laboratory tests
- Na, K, Cl, HCO3, Ca, Mg, BUN, Cr
3. Coagulation laboratory tests
- Fibrinogen, PT, PTT
4. Patient characteristics
- Gender, race
5. Injury features
- Liver/Spleen AIS, GCS, FAST scores 
6. Visit time

# Evaluation Targets
Compare values for 3 blood drawing laboratory tests on all visits
1. Hemoglobin (Hgb)
2. Platelet Count (Plt)
3. White Blood Cells (WBC)

# Preliminary Model
This projects aims to develop Yu’s LSTM model (2020). We use one LSTM module for generating lab test embeddings. When combing all features in the further linear layer, the model also incorporates patient embedding and injury embedding. Moreover, we also use multi-task learning for predicting checking probability and lab value.


