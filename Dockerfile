FROM python:3.9

WORKDIR /lab_reduction

COPY . /lab_reduction

RUN pip3 install pandas
RUN pip3 install torch torchvision torchaudio
RUN pip3 install -U scikit-learn


CMD ["python3","-u", "exp3.py"]
#CMD python3 exp3.py
#ENTRYPOINT [ "./exp3.py" ]